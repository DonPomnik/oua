package com.example.lab5;

import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class MainActivity extends FragmentActivity implements CharacterListFragment.CharacterListListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    public void itemClicked(Long id) {
     CharacterDetailFragment detailFragment = new CharacterDetailFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        detailFragment.setBowId(id);
        ft.replace(R.id.fragment_container,detailFragment);
        ft.addToBackStack(null);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
    }
}
