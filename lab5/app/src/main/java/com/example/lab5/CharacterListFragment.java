package com.example.lab5;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;


/**
 * A simple {@link Fragment} subclass.
 */
public class CharacterListFragment extends ListFragment {

    static interface CharacterListListener{
        void itemClicked(Long id);
    }
    private CharacterListListener listListener;
    public CharacterListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        String[] names = new String[Characters.BOWS.length];
        for (int i=0;i<names.length;i++){
            names[i]= Characters.BOWS[i].getName();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(inflater.getContext(), android.R.layout.simple_list_item_1, names);
        setListAdapter(adapter);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.listListener = (CharacterListListener) context;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        if(listListener!=null){
            listListener.itemClicked(id);
        }
    }
}
