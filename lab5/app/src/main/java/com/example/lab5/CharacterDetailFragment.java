package com.example.lab5;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class CharacterDetailFragment extends Fragment {
    private long bowId;

    public CharacterDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(savedInstanceState != null){
            bowId = savedInstanceState.getLong("bowId");
        }
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        photoFragment stopwatchFragment = new photoFragment();
        ft.replace(R.id.stopwatch_container, stopwatchFragment);
        ft.addToBackStack(null);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
        return inflater.inflate(R.layout.fragment_charakter2_detail, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        View view =getView();
        if(view!=null){
            TextView title = (TextView) view.findViewById(R.id.textName);
            Characters characters = Characters.BOWS[(int) bowId];
            title.setText(characters.getName());
            TextView desc = (TextView) view.findViewById(R.id.textDescription);
            desc.setText(characters.getDesc());
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putLong("bowId",bowId);
    }

    public void setBowId(long bowId) {
        this.bowId = bowId;
    }
}
