package com.example.lab5;

public class Characters {
    private String name;
    private String desc;

    public Characters(String name, String desc) {
        this.name = name;
        this.desc = desc;

    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }


    @Override
    public String toString() {
        return this.name;
    }

    public static final Characters[] BOWS ={
            new Characters("Lightning","Główną protagonistką Final Fantasy XIII, jak również narratorką i tymczasowo grywalną postacią w Final Fantasy XIII-2."),
            new Characters("Hope","Poznajemy go gdy ma 15 lat. Przypadkowo zostaje wciągnięty w akcję, kiedy to Lightning zatrzymuje pociąg, w którym był razem z matką."),
            new Characters("Snow", "Snow to umięśniony mężczyzna, narzeczony Serah.")
    };
}
