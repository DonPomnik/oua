package com.example.lab5;

public class Character2 {
    private String name;
    private String desc;


    public static final Character2[] ARROWS = {
      new Character2("Aluminium","Strzała wykonana z aluminium"),
            new Character2("Drewno","Strzała wykonana z drewna"),
            new Character2("Włókno szklane","Strzała wykonana z włókna szklanego")
    };

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }



    public Character2(String name, String desc) {
        this.name = name;
        this.desc = desc;

    }

    @Override
    public String toString() {
        return this.name;
    }
}
