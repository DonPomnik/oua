package com.hfad.lab7;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

public class PierwszoplanoweActivity extends AppCompatActivity {

    public static final String  EXTRA_DRUG0 ="bowNo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pierwszoplanowe);

        int bowNo = (Integer) getIntent().getExtras().get(EXTRA_DRUG0);
        try {
            SQLiteOpenHelper bowDatabaseHelper = new SQLHelper2(this);
            SQLiteDatabase db = bowDatabaseHelper.getWritableDatabase();
            Cursor cursor = db.query("DRUGOPLANOWE",new String[]{"NAME","DESCRIPTION","IMAGE_RESOURCE_ID","FAVORITE"},
                    "_id=?",new String[]{Integer.toString(bowNo)},
                    null,null,null);
            if(cursor.moveToFirst()){
                    String nameText = cursor.getString(0);
                    String descText = cursor.getString(1);
                    int photoId = cursor.getInt(2);
                    boolean isFavorite = (cursor.getInt(3)==1);
                    TextView nameField = (TextView) findViewById(R.id.nameField);
                    nameField.setText(nameText);

                    TextView descField = (TextView) findViewById(R.id.descField);
                    descField.setText(descText);

                   /* ImageView imageView = (ImageView) findViewById(R.id.imageView2);
                    imageView.setImageResource(photoId);
                    imageView.setContentDescription(nameText);*/

                CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox);
                checkBox.setChecked(isFavorite);
            }
            cursor.close();
            db.close();
        }catch (SQLException e){
            Toast toast = Toast.makeText(this, "baza niedostępna",Toast.LENGTH_SHORT);
            toast.show();
        }

    }
    public void onFavClicked(View view){
        int bowNo = (Integer) getIntent().getExtras().get(EXTRA_DRUG0);
        new UpdateDrinkTask().execute(bowNo);
    }

    private class UpdateDrinkTask extends AsyncTask<Integer, Void, Boolean>{
        ContentValues contentValues;
        @Override
        protected void onPreExecute() {
            CheckBox fav = (CheckBox) findViewById(R.id.checkBox);
            contentValues = new ContentValues();
            contentValues.put("FAVORITE",fav.isChecked());
        }

        @Override
        protected Boolean doInBackground(Integer... bows) {
            int bowNo= bows[0];
            SQLiteOpenHelper sqLiteOpenHelper = new SQLHelper2(PierwszoplanoweActivity.this);
            try {
                SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();
                db.update("BOW",contentValues,"_id=?",new String[]{Integer.toString(bowNo)});
                db.close();
                return true;
            }catch (SQLException e){
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(!aBoolean){
                Toast toast = Toast.makeText(PierwszoplanoweActivity.this,"baza danych niedostępna",Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }
}
