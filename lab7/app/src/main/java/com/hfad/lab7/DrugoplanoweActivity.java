package com.hfad.lab7;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DrugoplanoweActivity extends AppCompatActivity {
    public static final String  EXTRA_DRUG0 ="charNo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drugoplanowe);

        int charNo = (Integer) getIntent().getExtras().get(EXTRA_DRUG0);
        try {
            SQLiteOpenHelper bowDatabaseHelper = new SQLHelper(this);
            SQLiteDatabase db = bowDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.query("DRUGOPLANOWE",new String[]{"NAME","DESCRIPTION","IMAGE_RESOURCE_ID"},
                    "_id=?",new String[]{Integer.toString(charNo)},
                    null,null,null);
            if(cursor.moveToFirst()){
                String nameText = cursor.getString(0);
                String descText = cursor.getString(1);
                int photoId = cursor.getInt(2);

                TextView xd = (TextView) findViewById(R.id.abc);
                xd.setText(nameText);

                TextView x2= (TextView) findViewById(R.id.cba);
                x2.setText(descText);

               ImageView imageView = (ImageView) findViewById(R.id.imageView2);
                imageView.setImageResource(photoId);
                imageView.setContentDescription(nameText);


            }
            cursor.close();
            db.close();
        }catch (SQLException e){
            Toast toast = Toast.makeText(this, "baza niedostępna",Toast.LENGTH_SHORT);
            toast.show();
        }

    }
}
