package com.hfad.lab7;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLHelper extends SQLiteOpenHelper {

    public static final String DB_NAME ="pierwszopolanowe";
    public static final int DB_VERSION = 3;

    public SQLHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        updateMyDatabase(db, 0 , DB_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        updateMyDatabase(db,oldVersion,newVersion);
    }


    private void updateMyDatabase(SQLiteDatabase db, int oldVersion, int newVersion){
        if (oldVersion<1){
            db.execSQL("CREATE TABLE PIERWSZ (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "NAME TEXT, "
                    + "DESCRIPTION TEXT, "
                    + "IMAGE_RESOURCE_ID INTEGER,FAVORITE NUMERIC);");
            insert(db, "Lightning", "Prawdziwe imię Claire Farron. Stara się uratować swoją młodszą siostrę, Serah, ale zostaje wplątana w serię zdarzeń, które zagrażają jej domowi - Cocoon.", R.drawable.lightning);
            insert(db, "Snow", "Snow to umięśniony mężczyzna, narzeczony Serah.", R.drawable.snow);
            insert(db, "Vanille", "Jest to tajemnicza dziewczyna z rudymi włosami spiętymi w kucyki. Pomimo wielu trudności jest wciąż żywą i troskliwą osobą; jednakże jest wesołość służy do zakrycia win, które czuje od ponad pięciuset lat.", R.drawable.vanille);
        }else if(oldVersion<2){
            db.execSQL("ALTER TABLE PIERWSZ ADD COLUMN FAVORITE NUMERIC;");
        }
    }
    public static void insert(SQLiteDatabase db, String name, String desc, int resourceId){
        ContentValues contentValues = new ContentValues();
        contentValues.put("NAME", name);
        contentValues.put("DESCRIPTION",desc);
        contentValues.put("IMAGE_RESOURCE_ID",resourceId);
        db.insert("PIERWSZ",null,contentValues);
    }
}
