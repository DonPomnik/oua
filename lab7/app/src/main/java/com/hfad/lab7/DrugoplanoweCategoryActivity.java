package com.hfad.lab7;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class DrugoplanoweCategoryActivity extends ListActivity {

    private SQLiteDatabase db;
    private Cursor cursor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ListView listView = getListView();
        try{
            SQLiteOpenHelper char2SQLHelper = new SQLHelper(this);

            db = char2SQLHelper.getReadableDatabase();
            cursor = db.query("DRUGOPLANOWE",new String[]{"_id","NAME"},null,
                    null,null,null,null);
            CursorAdapter listAdapter = new SimpleCursorAdapter(this,android.R.layout.simple_expandable_list_item_1,
                    cursor,new String[]{"NAME"},new int[]{android.R.id.text1},0);
            listView.setAdapter(listAdapter);
        }catch (SQLException e){
            Toast toast = Toast.makeText(this, "Baza danych niedostępna",Toast.LENGTH_SHORT);
            toast.show();
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        cursor.close();
        db.close();
    }
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Intent intent = new Intent(DrugoplanoweCategoryActivity.this, DrugoplanoweActivity.class);
        intent.putExtra(DrugoplanoweActivity.EXTRA_DRUG0,(int)id);
        startActivity(intent);
    }

}
