package com.hfad.lab7;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLHelper2 extends SQLiteOpenHelper {

    public static final String DB_NAME ="drugoplanowe";
    public static final int DB_VERSION = 3;

    public SQLHelper2(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        updateMyDatabase(db, 0 , DB_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        updateMyDatabase(db,oldVersion,newVersion);
    }


    private void updateMyDatabase(SQLiteDatabase db, int oldVersion, int newVersion){
        if (oldVersion<1){
            db.execSQL("CREATE TABLE DRUGOPLANOWE (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "NAME TEXT, "
                    + "DESCRIPTION TEXT, "
            + "IMAGE_RESOURCE_ID INTEGER,FAVORITE NUMERIC);");
            insert(db, "Serah", "Jest młodszą siostrą Lightning o trzy lata oraz narzeczoną Snowa Villiers'a . Lightning oskarża Snowa o to że jej nie ochroni; również nie akceptuje ich związku co zmienia sięz czasem.", R.drawable.luk2);
            insert(db, "Nora", "Nora Estheim jest mało ważną postacią i matką Hope'a w Final Fantasy XIII.", R.drawable.luk8);
            insert(db, "Gadot", "Easter egg - brat opisu ;P", R.drawable.luk12);
        }else if(oldVersion<3){
            db.execSQL("ALTER TABLE DRUGOPLANOWE ADD COLUMN FAVORITE NUMERIC;");
        }
    }
    public static void insert(SQLiteDatabase db, String name, String desc, int resourceId){
        ContentValues contentValues = new ContentValues();
        contentValues.put("NAME", name);
        contentValues.put("DESCRIPTION",desc);
        contentValues.put("IMAGE_RESOURCE_ID",resourceId);
        db.insert("DRUGOPLANOWE",null,contentValues);
    }
}
