package com.example.lab4ver2;

public class Pierwszoplanowe {
    private String name;
    private String desc;
    private int imgRes;

    public Pierwszoplanowe(String name, String desc, int imgRes) {
        this.name = name;
        this.desc = desc;
        this.imgRes = imgRes;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public int getImgRes() {
        return imgRes;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public static final Pierwszoplanowe[] PIERWSZOPLANOWE ={
            new Pierwszoplanowe("Lightning","Prawdziwe imię Claire Farron (Eclair Farron w wersji japońskiej), jest główną protagonistką Final Fantasy XIII, jak również narratorką i tymczasowo grywalną postacią w Final Fantasy XIII-2. Powraca jako główna i jedyna grywalna postać w grze Lightning Returns: Final Fantasy XIII. W Final Fantasy XIII stara się uratować swoją młodszą siostrę, Serah.", R.drawable.lightning),
            new Pierwszoplanowe("Snow","Postać występująca we wszystkich trzech częściach Final Fantasy XIII, ale grywalna tylko w jej pierwszej odsłonie. Snow to umięśniony mężczyzna, narzeczony Serah.",R.drawable.snow),
            new Pierwszoplanowe("Hope", "Występuje zarówno w Final Fantasy XIII, jak i Final Fantasy XIII-2 i Final Fantasy XIII : Lightining's Return, ale jedynie tylko w tej pierwszej jest postacią grywalną. Poznajemy go gdy ma 15 lat. Przypadkowo zostaje wciągnięty w akcję, kiedy to Lightning zatrzymuje pociąg, w którym był razem z matką. Później obwinia Snow'a za śmierć matki, co jest jego główną motywacją do jakiegokolwiek działania.",R.drawable.hope),
            new Pierwszoplanowe("Vanille", "Jest to tajemnicza dziewczyna z rudymi włosami spiętymi w kucyki. Pomimo wielu trudności jest wciąż żywą i troskliwą osobą; jednakże jest wesołość służy do zakrycia win, które czuje od ponad pięciuset lat.", R.drawable.vanille)
    };
}
