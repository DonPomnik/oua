package com.example.lab4ver2;

public class Drugoplanowe {
    private String name;
    private String desc;
    private int imgRes;

    public static final Drugoplanowe[] DRUGOPLANOWE = {
      new Drugoplanowe("Serah","Jest młodszą siostrą Lightning o trzy lata oraz narzeczoną Snowa Villiers'a . Lightning oskarża Snowa o to że jej nie ochroni; również nie akceptuje ich związku co zmienia sięz czasem.\n" +
              "W Final Fantasy XIII, Serah jest pierwszą Pulse l'Cie w Cocoon , a jej przeznaczenie jest jednym z głownych histori gry.", R.drawable.serah),
            new Drugoplanowe("Nora","Mało ważna postać i matka Hope'a w Final Fantasy XIII. W wersji japońskiej głosu użycza jej Komina Matsushita.", R.drawable.nora),
            new Drugoplanowe("Gadot","Członek Drużyny Nora i przyjaciel Snow'a od jego najmłodszych lat. Jest ciemnoskórym mężczyzną z rudymi włosami i ubraniami z cyraneczki. Jego wygląd bazuje na modzie NBA i Hip-Hop'u.",R.drawable.gadot),
            new Drugoplanowe("Lebreau", "kobieta mająca czarne włosy spięte w kucyk i tatuaż w kształcie motyla na ramieniu. Jest równiez członkinią Drużyny Nora. Jej strój bazuje na wyglądzie graczy piłki siatkowej, ubranej w krótkie spodenki i tanktop podobny do koszuli z bufoniastymi rękawami. Jej bronią jest strzelba i pracuje jako barmanka w Bodhum.", R.drawable.lebrau)
    };

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public int getImgRes() {
        return imgRes;
    }

    public Drugoplanowe(String name, String desc, int imgRes) {
        this.name = name;
        this.desc = desc;
        this.imgRes = imgRes;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
