package com.example.lab4ver2;

import android.app.ActionBar;
import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

public class InfoActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        TextView toolbarText = (TextView) findViewById(R.id.textView2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar2);
        if(toolbarText!=null && toolbar!=null) {
            toolbarText.setText(getTitle());
            //setSupportActionBar(toolbar);
        }
        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText("aplikacja na zaliczenie");
        //ActionBar actionBar = getActionBar();
        //actionBar.setDisplayHomeAsUpEnabled(true);
    }
}
