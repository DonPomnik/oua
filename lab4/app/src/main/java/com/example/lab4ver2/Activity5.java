package com.example.lab4ver2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

public class Activity5 extends AppCompatActivity {

    public static final String EXTRA_ARROW0 = " ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_5);
        int arrno = (Integer) getIntent().getExtras().get(EXTRA_ARROW0);
        Drugoplanowe drugoplanowe = Drugoplanowe.DRUGOPLANOWE[arrno];

        TextView toolbarText = (TextView) findViewById(R.id.textView2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar2);
        if(toolbarText!=null && toolbar!=null) {
            toolbarText.setText(getTitle());
            //setSupportActionBar(toolbar);
        }

        ImageView photo = (ImageView)findViewById(R.id.photo1);
        photo.setImageResource(drugoplanowe.getImgRes());
        photo.setContentDescription(drugoplanowe.getName());

        TextView name = (TextView) findViewById(R.id.name1);
        name.setText(drugoplanowe.getName());

        TextView desc = (TextView) findViewById(R.id.desc1);
        desc.setText(drugoplanowe.getDesc());
        /*getSupportActionBar().setDisplayShowTitleEnabled(true);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);*/
    }
}
