package com.example.lab4ver2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView toolbarText = (TextView) findViewById(R.id.textView2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar2);
        if(toolbarText!=null && toolbar!=null) {
            toolbarText.setText("");
            setSupportActionBar(toolbar);
        }
        AdapterView.OnItemClickListener item = new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> listView, View view, int position, long id) {
                if(position == 0){
                    Intent intent = new Intent(MainActivity.this, Activity2.class);
                    startActivity(intent);
                }else if(position ==1){
                    Intent intent1 = new Intent(MainActivity.this, Activity3.class);
                    startActivity(intent1);
                }
            }
        };
        ListView listView = (ListView) findViewById(R.id.lista);
        listView.setOnItemClickListener(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(MainActivity.this,InfoActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
