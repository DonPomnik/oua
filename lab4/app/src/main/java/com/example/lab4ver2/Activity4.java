package com.example.lab4ver2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

public class Activity4 extends AppCompatActivity {


    public static final String EXTRA_BOW0 = " ";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_4);

        TextView toolbarText = (TextView) findViewById(R.id.textView2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar2);
        if(toolbarText!=null && toolbar!=null) {
            toolbarText.setText(getTitle());
            //setSupportActionBar(toolbar);
        }

        int bowno = (Integer) getIntent().getExtras().get(EXTRA_BOW0);
        Pierwszoplanowe bow = Pierwszoplanowe.PIERWSZOPLANOWE[bowno];

        ImageView photo = (ImageView)findViewById(R.id.photo);
        photo.setImageResource(bow.getImgRes());
        photo.setContentDescription(bow.getName());

        TextView name = (TextView) findViewById(R.id.name);
        name.setText(bow.getName());

        TextView desc = (TextView) findViewById(R.id.desc);
        desc.setText(bow.getDesc());
        /*getSupportActionBar().setDisplayShowTitleEnabled(true);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);*/
    }


}
