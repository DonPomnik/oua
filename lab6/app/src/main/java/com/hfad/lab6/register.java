package com.hfad.lab6;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class register extends AppCompatActivity {
    private final AppCompatActivity activity = register.this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Intent intent = getIntent();
    }
    SqlHelper sqlHelper = new SqlHelper(activity);
    public void reg(View view){
        EditText pasField = (EditText) findViewById(R.id.Pass2);
        String pass = pasField.getText().toString().trim();

        EditText usrField = (EditText) findViewById(R.id.Login2);
        String usr = usrField.getText().toString().trim();
        if(usr.equalsIgnoreCase("")){
            Context context = getApplicationContext();
            CharSequence text = "Podaj Login!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }else if(pass.equalsIgnoreCase("")){
            Context context = getApplicationContext();
            CharSequence text = "Podaj Hasło!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }else{
            if(!sqlHelper.checkUser(usr,pass)) {
                User user = new User(usr, pass);
                Context context = getApplicationContext();
                CharSequence text = usr + " "+pass;
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                 sqlHelper.addUser(user);
            }else {
                Context context = getApplicationContext();
                CharSequence text = "coś nie dziala!";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        }
    }
}
