package com.hfad.lab6;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class login extends AppCompatActivity {
    private final AppCompatActivity activity = login.this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Intent intent = getIntent();
    }
    SqlHelper sqlHelper = new SqlHelper(activity);
    public void log(View view){
        EditText usrField = (EditText) findViewById(R.id.Login1);
        String usr = usrField.getText().toString();

        EditText pasField = (EditText) findViewById(R.id.Pass1);
        String pass =pasField.getText().toString();


        if(usr.equalsIgnoreCase("")){
            Context context = getApplicationContext();
            CharSequence text = "Podaj Login!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }else if(pass.equalsIgnoreCase("")){
            Context context = getApplicationContext();
            CharSequence text = "Podaj Hasło!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }else{

            if(sqlHelper.checkUser(usr,pass)){
              Intent intent = new Intent(activity, Yay.class);
              startActivity(intent);
            }
        }
    }
}
