package com.hfad.lab6;

public class User {

    private long id;
    private String login;
    private String pass;

    public User( String login, String pass) {

        this.login = login;
        this.pass = pass;
    }

    public User() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
