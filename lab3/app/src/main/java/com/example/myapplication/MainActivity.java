package com.example.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        selectSex();
    }

    public void przejscie(View view){
        EditText messageView = (EditText)findViewById(R.id.nick);
        String messateText = messageView.getText().toString();
        Intent intent = new Intent(this, wynik.class);
        intent.putExtra(wynik.EXTRA_MESSAGE, messateText);

        //startActivity(intent);
        startActivity(intent);


    }

    public void przejsice_2(View view){
        EditText messageView2 = (EditText)findViewById(R.id.name);
        String messateText2 = messageView2.getText().toString();
        Intent intent2 = new Intent(this, wynik.class);
        intent2.putExtra(wynik.EXTRA_MESSAGE2, messateText2);
        startActivity(intent2);
    }

    private RadioGroup radioSex;
    private RadioButton radioSexBut;
    private Button btnDisplay;

    public void selectSex(){
        radioSex = (RadioGroup) findViewById(R.id.radioSex);
        btnDisplay = (Button) findViewById(R.id.btnDisplay);

        btnDisplay.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick( View v){
                int selectedId = radioSex.getCheckedRadioButtonId();

                radioSexBut= (RadioButton) findViewById(selectedId);

                Toast.makeText(MainActivity.this, radioSexBut.getText(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
