package com.example.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

public class wynik extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "";
    public static final String EXTRA_MESSAGE2 = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wynik);
        if(EXTRA_MESSAGE!=null){
            Intent intent = getIntent();
            String messageText = intent.getStringExtra(EXTRA_MESSAGE);
            TextView messageView = (TextView)findViewById(R.id.wynik);
            messageView.setText("Wybrana nazwa: " +messageText );
        }

        else if(EXTRA_MESSAGE2!=null) {

            Intent intent2 = getIntent();
            String messageText2 = intent2.getStringExtra(EXTRA_MESSAGE2);
            TextView messageView2 = (TextView) findViewById(R.id.wynik2);

            messageView2.setText("ImieWybrana nazwa: " + messageText2);
        }
    }

    public void przejscie2(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }


}
