package com.example.aktywnosci;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

public class InfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }
        public void aktywnosc(View view){
            Intent intent = new Intent(this, InfoActivity.class);
            startActivity(intent);
        }

        public void domowa(View view){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }

    public void onSendMessage(View view){
        EditText messageView = (EditText)findViewById(R.id.message);
        String messateText = messageView.getText().toString();
        Intent intent = new Intent(this, ReceiveMessageActivity.class);
        intent.putExtra(ReceiveMessageActivity.EXTRA_MESSAGE, messateText);
        startActivity(intent);
    }

}
