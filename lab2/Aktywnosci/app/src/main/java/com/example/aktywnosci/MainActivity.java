package com.example.aktywnosci;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void onSendMessage(View view){
        EditText messageView = (EditText)findViewById(R.id.message);
        String messateText = messageView.getText().toString();
        Intent intent = new Intent(this, ReceiveMessageActivity.class);
        intent.putExtra(ReceiveMessageActivity.EXTRA_MESSAGE, messateText);
        startActivity(intent);
    }

    public void aktywnosc(View view){
        Intent intent = new Intent(this, InfoActivity.class);
        startActivity(intent);
    }
}
