package com.example.kalkulator_domnik;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void dodajLiczby(View view) {
        TextView wynik = (TextView) findViewById(R.id.wynik);
        Spinner color = (Spinner) findViewById(R.id.color);
        int var1 = Integer.parseInt(String.valueOf(color.getSelectedItem()));
        Spinner color2 = (Spinner) findViewById(R.id.color2);
        int var2 = Integer.parseInt(String.valueOf(color2.getSelectedItem()));
        int wyn = var1 + var2;
        wynik.setText(Integer.toString(wyn));
    }

    public void odejminLiczby(View view){
        TextView wynik = (TextView) findViewById(R.id.wynik);
        Spinner color = (Spinner) findViewById(R.id.color);
        int var1 = Integer.parseInt(String.valueOf(color.getSelectedItem()));
        Spinner color2 = (Spinner) findViewById(R.id.color2);
        int var2 = Integer.parseInt(String.valueOf(color2.getSelectedItem()));
        int wyn = var1-var2;
        wynik.setText(Integer.toString(wyn));
        }
    public void pomnozLiczby(View view){
        TextView wynik = (TextView) findViewById(R.id.wynik);
        Spinner color = (Spinner) findViewById(R.id.color);
        int var1 = Integer.parseInt(String.valueOf(color.getSelectedItem()));
        Spinner color2 = (Spinner) findViewById(R.id.color2);
        int var2 = Integer.parseInt(String.valueOf(color2.getSelectedItem()));
        int wyn = var1*var2;
        wynik.setText(Integer.toString(wyn));
        }
    public void podzielLiczby(View view){
        TextView wynik = (TextView) findViewById(R.id.wynik);
        Spinner color = (Spinner) findViewById(R.id.color);
        int var1 = Integer.parseInt(String.valueOf(color.getSelectedItem()));
        Spinner color2 = (Spinner) findViewById(R.id.color2);
        int var2 = Integer.parseInt(String.valueOf(color2.getSelectedItem()));
        int wyn;
        if(var1!=0&&var2!=0) {
            wyn = var1 / var2;
        }else{
            wyn =0;
        }
        wynik.setText(Integer.toString(wyn));
        }
}
